#!/usr/bin/env perl

use strict;
use warnings;

use CodePrettifier::CodeParser;
use CodePrettifier::HtmlWriter;

# language passed to CodeParser contructor must be 
# one of the languages in CodePrettifier/Languages
my $language = 'Perl' 
my $codeparser = CodeParser->new($language);

# Here you need real code, perhaps the contents of a
# file, or anything else.
my $somecode = ''; 
$codeparser->source_code($somecode)

# doing it's work
$codeparser->parse_code();

# Here you will see the the source code with lot of
# tags on it. Something like:
# <RESERVED>if</RESERVED> var == <STRING>'some stringq</STRING>
print $codeparser->parsed_code();

# Now, writting a html
my $htmlwriter = HtmlWriter->new();

# htmlwriter gets a parsed code from codeparser
# and do an html. do_html() has some options wich are passed
# as a hash
$htmlwriter->do_html($codeparser->parsed_code(), 
		     {style_inline => 0,
		      line_number => 1,
		      full_html => 1,
		     });

# prints the html generated on do_html
print $htmlwriter->html_code();

# and you can write a file
my $outfile = 'out.html';
$htmlwriter->write_html($outfile);
