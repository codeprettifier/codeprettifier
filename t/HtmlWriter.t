#!/usr/bin/env perl

use strict;
use warnings;

use feature 'say';
use Test::Simple tests => 5;
use CodePrettifier::HtmlWriter;


say 'Testing HtmlWriter';

my $htmlwriter = HtmlWriter->new();

say "  Testing class on elements";
my $parsed_code = '<STRING>"some string"</STRING>';
my $html_code = '<div id="source_code"><pre><span class="string">';
$html_code .= '"some string"</span></pre></div>';
$htmlwriter->do_html($parsed_code);
ok($htmlwriter->html_code() eq $html_code);

say "  Testing line numbers";
$parsed_code = '<STRING>"some string"</STRING>';
$html_code = '<div id="source_code"><pre><span class="line_number">1';
$html_code .= '</span><span class="string">"some string"</span>'."\n</pre></div>";
$htmlwriter->do_html($parsed_code, {line_number => 1});
ok($htmlwriter->html_code() eq $html_code);

say "  Testing inline style";
$parsed_code = '<STRING>"some string"</STRING>';
$html_code = '<div style=" background: #000; color: ';
$html_code .= '#05fc05; "><pre><span style=" color: #FDF5E6; ">';
$html_code .= '"some string"</span></pre></div>';
$htmlwriter->do_html($parsed_code, {style_inline => 1});
ok($htmlwriter->html_code() eq $html_code);

say "  Testing inline style with reserved";
$parsed_code = '<RESERVED>if</RESERVED>';
$html_code = '<div style=" background: #000; color: ';
$html_code .= '#05fc05; "><pre><span style=" color: #5dc8c7;';
$html_code .= ' font-weight: bold; ">if</span></pre></div>';
$htmlwriter->do_html($parsed_code, {style_inline => 1});
ok($htmlwriter->html_code() eq $html_code);


say "  Testing full html";
$parsed_code = '<RESERVED>if</RESERVED>';
$htmlwriter->do_html($parsed_code, {full_html => 1});
$htmlwriter->html_code() =~ /(class="sourcecode2html")/;
my $ok = $1 ? 1 : 0;
ok($ok);
