#!/usr/bin/env perl

use strict;
use warnings;

use feature 'say';
use Test::Simple tests => 5;
use CodePrettifier::CodeParser;


say "Testing language Perl";

my $TEST_DESCRIPTION;

my $LANGUAGE = 'Perl';
my $codeparser = CodeParser->new($LANGUAGE);

$TEST_DESCRIPTION =  '  Testing contructor for Perl';
ok(ref($codeparser->language()) eq 'Perl', $TEST_DESCRIPTION);

$TEST_DESCRIPTION =  '  Testing tagify strings';
my ($codeline, $parsed_line);

# First with strings delimitated by single quotes.
$codeline = "a = 'Hello, world'";
$parsed_line = "a = <STRING>'Hello, world'</STRING>\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

# Now with double quotes.
$codeline = 'a = "Hello, world"';
$parsed_line = 'a = <STRING>"Hello, world"</STRING>' . "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION =  '  Testing tagify variables';
$codeline = '$asdf = 1';
$parsed_line = '$<VARIABLE>asdf</VARIABLE> = 1' . "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION =  '  Testing comments';
$codeline = '1 = 1 #really?';
$parsed_line = '1 = 1 <COMMENT>#really?</COMMENT>' . "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

