#!/usr/bin/env perl

use strict;
use warnings;

use feature 'say';
use Test::Simple tests => 5;
use CodePrettifier::CodeParser;


say "Testing language Ruby";

my $LANGUAGE = 'Ruby';
my $codeparser = CodeParser->new($LANGUAGE);

my ($source_code, $parsed_code);
my $TEST_DESCRIPTION;

$TEST_DESCRIPTION = '  Testing contructor for Ruby';
ok(ref($codeparser->language()) eq 'Ruby', $TEST_DESCRIPTION);


$TEST_DESCRIPTION = 'Testing camel case';
$source_code = 'CamelCase';
$parsed_code = '<TYPENAME>CamelCase</TYPENAME>'."\n";
$codeparser->source_code($source_code);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = "  Testing double quote strings";
$source_code = '"some string"';
$parsed_code = '<STRING>"some string"</STRING>';
$parsed_code .=  "\n";
$codeparser->source_code($source_code);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = "  Testing single quote strings";
$source_code = "'some string'";
$parsed_code = "<STRING>'some string'</STRING>";
$parsed_code .=  "\n";
$codeparser->source_code($source_code);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = "  Testing function names";
$source_code = "def some_function()";
$parsed_code = "<KEYWORD>def</KEYWORD> <FUNCTIONNAME>some_function</FUNCTIONNAME>()";
$parsed_code .=  "\n";
$codeparser->source_code($source_code);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);
