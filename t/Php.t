#!/usr/bin/env perl

use strict;
use warnings;

use feature 'say';
use Test::Simple tests => 8;
use CodePrettifier::CodeParser;


say "Testing language Php";

my $LANGUAGE = 'Php';
my $codeparser = CodeParser->new($LANGUAGE);

my $TEST_DESCRIPTION;
my ($codeline, $parsed_line);

$TEST_DESCRIPTION = '  Testing contructor for Php -- ';
ok($codeparser->type() eq 'Php', $TEST_DESCRIPTION);

$codeline = "a = 'Hello, world'";
$parsed_line = "a = <STRING>'Hello, world'</STRING>\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line);

$codeline = 'a = "Hello, world"';
$parsed_line = 'a = <STRING>"Hello, world"</STRING>' . "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line);


$TEST_DESCRIPTION = '  Testing tagify variables -- ';
$codeline = '$asdf = 1';
$parsed_line = '$<VARIABLE>asdf</VARIABLE> = 1' . "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  Testing comments -- ';
$codeline = '1 = 1 //really?';
$parsed_line = '1 = 1 <COMMENT>//really?</COMMENT>' . "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  Testing reserved words';
$codeline = 'echo something';
$parsed_line = '<KEYWORD>echo</KEYWORD> something' . "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  Testing function names';
$codeline = 'function somefunc(';
$parsed_line = '<KEYWORD>function</KEYWORD> <FUNCTIONNAME>somefunc';
$parsed_line .= '</FUNCTIONNAME>(' . "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

# BROKEN TEST! Must be corrected changing behavior on escpaping html chars.
# See TODO.

# $TEST_DESCRIPTION = '  Testing coding init sign';
# $codeline = '<?php $somephpcode =';
# $parsed_line = '<KEYWORD>&lt;?php</KEYWORD> <VARIABLE>$somephpcode';
# $parsed_line .= '</VARIABLE> =' . "\n";
# $codeparser->source_code($codeline);
# $codeparser->parse_code();
# ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  Testing class names';
$codeline = 'class SomeClass{';
$parsed_line = '<KEYWORD>class</KEYWORD>';
$parsed_line .= ' <TYPENAME>SomeClass</TYPENAME>{' . "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

