#!/usr/bin/env perl

use strict;
use warnings;

use feature 'say';
use Test::Simple tests => 3;
use CodePrettifier::CodeParser;


say 'Testing CodeParser';

my ($source_code, $parsed_code, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  Testing constructor -- ';
my $codeparser = CodeParser->new();
ok($codeparser->type() eq 'Language', $TEST_DESCRIPTION);


$TEST_DESCRIPTION = '  Testing escape lesser and greater signs -- ';
$source_code = 'if($a > $b)';
$parsed_code ='if($a &gt; $b)';
$codeparser->source_code($source_code);
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  Testing source_code method -- ';
$source_code = 'if($something eq $otherthing)';
$codeparser->source_code($source_code);
ok($codeparser->parsed_code() eq $source_code, $TEST_DESCRIPTION);
