#!/usr/bin/env perl

use strict;
use warnings;

use Test::Simple tests => 6;
use CodePrettifier::CodeParser;


print  "Testing language Javascript\n";

my $LANGUAGE = 'Javascript';
my $codeparser = CodeParser->new($LANGUAGE);

my ($codeline, $parsed_line);
my $TEST_DESCRIPTION;

$TEST_DESCRIPTION = '  Testing contructor for Javascript';
ok($codeparser->type() eq 'Javascript');

$TEST_DESCRIPTION = '  Testing variable names';
$codeline = "var a = 1";
$parsed_line = "<KEYWORD>var</KEYWORD> <VARIABLE>a</VARIABLE> = 1\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  Testing // comments';
$codeline = "1 = 1 // really?";
$parsed_line = "1 = 1 <COMMENT>// really?</COMMENT>\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  single quotes string';
$codeline = "'some string'";
$parsed_line = "<STRING>'some string'</STRING>";
$parsed_line .= "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  double quotes string';
$codeline = '"some string"';
$parsed_line = '<STRING>"some string"</STRING>';
$parsed_line .= "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  testing function names';
$codeline = 'function someFunc(){';
$parsed_line = '<KEYWORD>function</KEYWORD> <FUNCTIONNAME>someFunc</FUNCTIONNAME>(){';
$parsed_line .= "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);
