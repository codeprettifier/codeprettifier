
#!/usr/bin/env perl

use strict;
use warnings;

use feature 'say';
use Test::Simple tests => 10;
use CodePrettifier::CodeParser;
use CodePrettifier::Languages::Java;


say "Testing language Java";

my $LANGUAGE = 'Java';

my ($sourcecode, $parsed_code);
my $TEST_DESCRIPTION;

my $codeparser = Java->new();
$TEST_DESCRIPTION = '  Testing contructor for Java';
ok(ref($codeparser) eq 'Java');

$codeparser = CodeParser->new($LANGUAGE);
$TEST_DESCRIPTION = '  Testing type names';
$sourcecode = 'String ;';
$parsed_code = '<TYPENAME>String</TYPENAME> ;' . "\n";
$codeparser->source_code($sourcecode);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  Testing function names';
$sourcecode = 'String bla(';
$parsed_code = '<TYPENAME>String</TYPENAME> <FUNCTIONNAME>bla</FUNCTIONNAME>(' . "\n";
$codeparser->source_code($sourcecode);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  Testing variable names';
$sourcecode = 'String a;';
$parsed_code = '<TYPENAME>String</TYPENAME> <VARIABLE>a</VARIABLE>;' . "\n";
$codeparser->source_code($sourcecode);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  Testing variable names (predefined types)';
$sourcecode = 'void a;';
$parsed_code = '<TYPENAME>void</TYPENAME> <VARIABLE>a</VARIABLE>;' . "\n";
$codeparser->source_code($sourcecode);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  Testing single quote stings';
$sourcecode =  "var = 'asdf'";
$parsed_code = "var = <STRING>'asdf'</STRING>";
$parsed_code .= "\n";
$codeparser->source_code($sourcecode);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  Testing double quote stings';
$sourcecode =  'var = "asdf"';
$parsed_code = 'var = <STRING>"asdf"</STRING>';
$parsed_code .= "\n";
$codeparser->source_code($sourcecode);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  Testing c-like comments';
$sourcecode =  "/* some comment //\nhere is part of comment */";
$parsed_code = "<COMMENT>/* some comment //\nhere is part of comment */</COMMENT>";
$parsed_code .= "\n";
$codeparser->source_code($sourcecode);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  Testing // comments';
$sourcecode =  "/* some comment */ bla bla //here is another comment";
$parsed_code = "<COMMENT>/* some comment */</COMMENT> bla bla <COMMENT>//here is another comment</COMMENT>";
$parsed_code .= "\n";
$codeparser->source_code($sourcecode);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  Testing function names';
$sourcecode =  "public void init()";
$parsed_code = "<KEYWORD>public</KEYWORD> <TYPENAME>void</TYPENAME> <FUNCTIONNAME>init</FUNCTIONNAME>()";
$parsed_code .= "\n";
$codeparser->source_code($sourcecode);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);
