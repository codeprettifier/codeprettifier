#!/usr/bin/env perl

use strict;
use warnings;

use feature 'say';
use Test::Simple tests => 2;
use CodePrettifier::CodeParser;
use CodePrettifier::Languages::SyntacticElements::CommentDelimiter;


my $comment_delimiter = CommentDelimiter->new();

my $TEST_DESCRIPTION = 'Testing _inside_comments - really inside';
my $content = '\/\/ something ';
my $code = '<COMMENT>/* comment // something bla */</COMMENT>';
$code .= "\n";
ok($comment_delimiter->_inside_comments($content, $code), $TEST_DESCRIPTION);


$TEST_DESCRIPTION = 'Testing _inside_comments - not inside';
$content = '\/\/ bla ';
$code = '<COMMENT>/* comment */</COMMENT> something // bla ';
$code .= "\n";
ok(!$comment_delimiter->_inside_comments($content, $code), $TEST_DESCRIPTION);
