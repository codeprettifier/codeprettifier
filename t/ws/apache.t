#!/usr/bin/env perl

use strict;
use warnings;

use feature 'say';
use Test::Simple tests => 4;

use HTTP::Request::Common qw(POST);
use LWP::UserAgent;

say 'Testing apache ws';

my $TEST_DESCRIPTION;
my $OK;
my $CODEPRETTIFIER_URL = 'http://localhost/ws/codeprettifier';

my $ua = LWP::UserAgent->new();
my $request;
my $response;
my $response_content;
my $source_code;

$TEST_DESCRIPTION = 'Testing  get request - help message';

$request = HTTP::Request->new(GET => $CODEPRETTIFIER_URL);
$response_content = $ua->request($request)->content();
$response_content =~ /(API params:)/;
$OK = (defined $1 && $1) ? 1 : 0;
ok($OK, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = 'Testing  post request with source code and language';

$source_code = 'def some_python_func(';
$request = HTTP::Request->new(POST => $CODEPRETTIFIER_URL);
$request->content("source_code=$source_code&language=Python");
$response_content = $ua->request($request)->content();
$response_content =~ /(class="reserved").+(class="functionname")/;
$OK = ((defined $1 && defined $2) && ($1 && $2)) ? 1 : 0;
ok($OK, $TEST_DESCRIPTION);


$TEST_DESCRIPTION = 'Testing  post request with source code, language and full_html';

$source_code = 'def some_python_func(';
$request = HTTP::Request->new(POST => $CODEPRETTIFIER_URL);
$request->content("source_code=$source_code&language=Python&full_html=1");
$response_content = $ua->request($request)->content();
$response_content =~ /(id="source_code")/;
$OK = (defined $1 && $1 eq 'id="source_code"') ? 1 : 0;
ok($OK, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = 'Testing  post url-encoded arguments';

$source_code = "if(%24var%20eq%20'something')";
$request = HTTP::Request->new(POST => $CODEPRETTIFIER_URL);
$request->content("source_code=$source_code&language=Perl&full_html=0");
$response_content = $ua->request($request)->content();
$response_content =~ /(%20eq)/;
$OK = (!defined $1 || $1 eq 'id="source_code"') ? 1 : 0;
ok($OK, $TEST_DESCRIPTION);
