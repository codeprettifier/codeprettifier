#!/usr/bin/env perl

use warnings;

use feature 'say';
use Test::Simple tests => 4;
use CodePrettifier::Languages::Language;
use CodePrettifier::Languages::Perl;
use CodePrettifier::Languages::Python;
use CodePrettifier::Languages::SyntacticElements::SyntacticElement;


say 'Testing Language.pm';

my ($language, $source_code, $parsed_code, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = 'Testing language constructor';
$language = Language->new();
ok(ref($language) eq 'Language', $TEST_DESCRIPTION);

$TEST_DESCRIPTION = 'Testing _clean_parsed_code';
$language = Perl->new();
$source_code = '<STRING>"<RESERVED>if</RESERVED> something"</STRING>';
$parsed_code = '<STRING>"if something"</STRING>';
ok($language->_clean_parsed_code($source_code) eq $parsed_code,
   $TEST_DESCRIPTION);

$language = Python->new();
$TEST_DESCRIPTION = '  Testing _clean_parsed_code';
$source_code = <<CODE;
<STRING>""" Make the checks before applying the rules for next generation.
    We need to prepare it before <FUNCTIONNAME>apply</FUNCTIONNAME> the rules on the cells
    otherwise we'll have wrong operations
"""</STRING>
CODE
$parsed_code = <<PARSED;
<STRING>""" Make the checks before applying the rules for next generation.
    We need to prepare it before apply the rules on the cells
    otherwise we'll have wrong operations
"""</STRING>
PARSED

ok($language->_clean_parsed_code($source_code) eq $parsed_code,
   $TEST_DESCRIPTION);

$language = Python->new();
$TEST_DESCRIPTION = '  Testing _clean_parsed_code';
$source_code = <<CODE;
# some comment '
a = 'a'
CODE

$parsed_code = <<PARSED;
<COMMENT># some comment '</COMMENT>
<VARIABLE>a</VARIABLE> = <STRING>'a'</STRING>
PARSED

$source_code = $language->parse_code($source_code);
ok($language->_clean_parsed_code($source_code) eq $parsed_code,
   $TEST_DESCRIPTION);


$TEST_DESCRIPTION = "Testing _create_syntactic_element";
$language = Language->new();
my @lista = ('something');
$language->_create_syntactic_element(\@lista, SyntacticElement);
ok(ref(${@{$language}[0]}[0]) eq 'SyntacticElement', $TEST_DESCRIPTION);
