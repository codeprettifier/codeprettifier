#!/usr/bin/env perl

use strict;
use warnings;

use feature 'say';
use Test::Simple tests => 7;
use CodePrettifier::CodeParser;


say "Testing language Bash";

my $LANGUAGE = 'Bash';
my $codeparser = CodeParser->new($LANGUAGE);

my ($codeline, $parsed_line);
my $TEST_DESCRIPTION;

$TEST_DESCRIPTION = '  Testing contructor for Bash';
ok(ref($codeparser->language()) eq 'Bash', $TEST_DESCRIPTION);

$TEST_DESCRIPTION = "  Testing double quote strings";
$codeline = '"some char arr"';
$parsed_line = '<STRING>"some char arr"</STRING>';
$parsed_line .=  "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = "  Testing single quote strings";
$codeline = "'some char arr'";
$parsed_line = "<STRING>'some char arr'</STRING>";
$parsed_line .=  "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = "  Testing comments";
$codeline = "#some comment";
$parsed_line = "<COMMENT>#some comment</COMMENT>";
$parsed_line .=  "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = "  Testing function names";
$codeline = "some_function(){";
$parsed_line = "<FUNCTIONNAME>some_function</FUNCTIONNAME>(){";
$parsed_line .=  "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = "  Testing variable delimiters";
$codeline = "VARIABLE=value;";
$parsed_line = "<VARIABLE>VARIABLE</VARIABLE>=value;";
$parsed_line .=  "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  Testing variable $ delimiters';
$codeline = '$some_var=1';
$parsed_line = '$<VARIABLE>some_var</VARIABLE>=1';
$parsed_line .=  "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);
