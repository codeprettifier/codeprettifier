#!/usr/bin/env perl

use strict;
use warnings;

use feature 'say';
use Test::Simple tests => 20;
use CodePrettifier::CodeParser;
use CodePrettifier::Languages::Python;


say "Testing language Python";

my $LANGUAGE = 'Python';
my $codeparser = Python->new();
ok(ref($codeparser) eq 'Python');

my ($codeline, $parsed_line);
my $TEST_DESCRIPTION;

$codeparser = CodeParser->new($LANGUAGE);

$TEST_DESCRIPTION = '  Testing contructor for Python';
ok($codeparser->type() eq 'Python');

$TEST_DESCRIPTION = '  Testing tagify strings';

# First with strings delimitated by single quotes.
$codeline = "a = 'Hello, world'";
$parsed_line = "<VARIABLE>a</VARIABLE> = <STRING>'Hello, world'</STRING>\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

# Now with double quotes.
$codeline = 'a = "Hello, world"';
$parsed_line = '<VARIABLE>a</VARIABLE> = <STRING>"Hello, world"</STRING>' . "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);


# Here, multi-line strings
$codeline = '""" Testing multiline
                 strings
             """';
$parsed_line = '<STRING>""" Testing multiline
                 strings
             """</STRING>' . "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

# Here, multi-line strings - bug with more than one
$codeline = '""" Testing multiline
                 strings
             """
""" another string
"""';
$parsed_line = '<STRING>""" Testing multiline
                 strings
             """</STRING>
<STRING>""" another string
"""</STRING>' . "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);


# Testing tagify_comments
$TEST_DESCRIPTION = '  Testing tagify comments';
$codeline = 'somestuff.ihavenoidea(); # I dont know how it works...';
$parsed_line = 'somestuff.ihavenoidea();';
$parsed_line .= ' <COMMENT># I dont know how it works...</COMMENT>' . "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);


$TEST_DESCRIPTION = '  Testing tagfy reserved words';
$codeline = 'for i in (1, 10):
                 yield i';

$parsed_line = '<KEYWORD>for</KEYWORD> i <KEYWORD>in</KEYWORD> (1, 10):
                 <KEYWORD>yield</KEYWORD> i' . "\n";

$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);


$TEST_DESCRIPTION = '  Testing tagfing function names';
$codeline = 'def some_func():';
$parsed_line = '<KEYWORD>def</KEYWORD>';
$parsed_line .= ' <FUNCTIONNAME>some_func</FUNCTIONNAME>():' . "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);


$TEST_DESCRIPTION = '  Testing built-in function names';
$codeline = 'a = int(1)';
$parsed_line = '<VARIABLE>a</VARIABLE> = <FUNCTIONNAME>int</FUNCTIONNAME>(1)' . "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  Testing class names';
$codeline = 'class SomeClass(';
$parsed_line = '<KEYWORD>class</KEYWORD>';
$parsed_line .= ' <TYPENAME>SomeClass</TYPENAME>(' . "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  Testing decorators';
$codeline = '@property';
$parsed_line = '<TYPENAME>@property</TYPENAME>' . "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  Testing % signs';
$codeline = '"%s" = % a';
$parsed_line = '<STRING>"%s"</STRING> = % a' . "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);


$TEST_DESCRIPTION = '  Testing two equal elements in line';
$codeline = "str(1) + str({})";
$parsed_line = "<FUNCTIONNAME>str</FUNCTIONNAME>(1) ";
$parsed_line .= "+ <FUNCTIONNAME>str</FUNCTIONNAME>({})" . "\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  Variable names';
$codeline = "CACHE = {}";
$parsed_line = "<VARIABLE>CACHE</VARIABLE> = {}\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  named args to func';
$codeline = "run(1, a=2, b='asdf')";
$parsed_line = "run(1, a=2, b=<STRING>'asdf'</STRING>)\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  named args to func multi-line';
$codeline = "
run(1,
    a=2,
    b='asdf')";
$parsed_line = "
run(1,
    a=2,
    b=<STRING>'asdf'</STRING>)\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  named args to func multi-line error with [';
$codeline = "
run(1,
    a=[],
    b='asdf')";
$parsed_line = "
run(1,
    a=[],
    b=<STRING>'asdf'</STRING>)\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  Testing async keyword';
$codeline = "async with cm:";
$parsed_line = "<KEYWORD>async</KEYWORD> <KEYWORD>with</KEYWORD> cm:\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  Testing await keyword';
$codeline = "await bla()";
$parsed_line = "<KEYWORD>await</KEYWORD> bla()\n";
$codeparser->source_code($codeline);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_line, $TEST_DESCRIPTION);
