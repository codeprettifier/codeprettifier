#!/usr/bin/env perl

use strict;
use warnings;

use feature 'say';
use Test::Simple tests => 3;

use CodePrettifier::Languages::SyntacticElements::SyntacticElement;

my ($element, $source_code, $parsed_code, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = 'Testing SyntacticElement constructor';
$element = SyntacticElement->new();
ok(ref($element) eq 'SyntacticElement', $TEST_DESCRIPTION);

$TEST_DESCRIPTION = 'Testing SyntacticElement escape_chars';
$element = SyntacticElement->new();
$source_code = '*, \, $, ?';
$parsed_code = '\*, \\, \$, \?';
ok($element->escape_chars($source_code) eq $parsed_code, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = 'Testing SyntacticElement unescape_chars';
$element = SyntacticElement->new();
$parsed_code = '*, \, $, ?';
$source_code = '\*, \\, \$, \?';
ok($element->unescape_chars($source_code) eq $parsed_code, $TEST_DESCRIPTION);




