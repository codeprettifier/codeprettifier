#!/usr/bin/env perl

use strict;
use warnings;

use feature 'say';
use Test::Simple tests => 12;
use CodePrettifier::CodeParser;



my ($sourcecode, $parsed_code);
my $TEST_DESCRIPTION;

say "Testing language C";

my $LANGUAGE = 'C';
my $codeparser = CodeParser->new($LANGUAGE);


$TEST_DESCRIPTION = '  Testing contructor for C -- ';
ok(ref($codeparser->language()) eq 'C', $TEST_DESCRIPTION);

$TEST_DESCRIPTION = '  Testing reserved words -- ';
$sourcecode = 'while(i=0;1<10;1++){';
$parsed_code = '<KEYWORD>while</KEYWORD>(i=0;1&lt;10;1++){' . "\n";
$codeparser->source_code($sourcecode);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = "  Testing comments -- ";
$sourcecode = 'c = b++ + b++ /* undefined behavior */';
$parsed_code = 'c = b++ + b++ <COMMENT>/* undefined behavior */</COMMENT>' . "\n";
$codeparser->source_code($sourcecode);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = "  Testing includes -- ";
$sourcecode = '#include <stdio.h>';
$parsed_code = "<FUNCTIONNAME>#include</FUNCTIONNAME> &lt;stdio.h&gt;" . "\n";
$codeparser->source_code($sourcecode);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = "  Testing predefined type names ";
$sourcecode = 'int ';
$parsed_code = '<TYPENAME>int</TYPENAME> ' . "\n";
$codeparser->source_code($sourcecode);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = "  Testing variable names ";
$sourcecode = 'int a;';
$parsed_code = '<TYPENAME>int</TYPENAME> <VARIABLE>a</VARIABLE>;' . "\n";
$codeparser->source_code($sourcecode);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = "  Testing variable names (more than one def) ";
$sourcecode = "\n".'     int a, b, c_d;';
$parsed_code = "\n".'     <TYPENAME>int</TYPENAME> <VARIABLE>a</VARIABLE>,';
$parsed_code .= ' <VARIABLE>b</VARIABLE>, <VARIABLE>c_d</VARIABLE>;' . "\n";
$codeparser->source_code($sourcecode);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);

$TEST_DESCRIPTION = "  Testing variable names (array) ";
$sourcecode = 'int arr[10];';
$parsed_code = '<TYPENAME>int</TYPENAME> <VARIABLE>arr</VARIABLE>[10];' . "\n";
$codeparser->source_code($sourcecode);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);


$TEST_DESCRIPTION = "  Testing variable names (with equals)";
$sourcecode = 'int a = 10;';
$parsed_code = '<TYPENAME>int</TYPENAME> <VARIABLE>a</VARIABLE> = 10;' ;
$parsed_code .=  "\n";
$codeparser->source_code($sourcecode);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);


$TEST_DESCRIPTION = "  Testing function names";
$sourcecode = 'int main(';
$parsed_code = '<TYPENAME>int</TYPENAME> <FUNCTIONNAME>main</FUNCTIONNAME>(' ;
$parsed_code .=  "\n";
$codeparser->source_code($sourcecode);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);


$TEST_DESCRIPTION = "  Testing double quote strings";
$sourcecode = '"some char arr"';
$parsed_code = '<STRING>"some char arr"</STRING>';
$parsed_code .=  "\n";
$codeparser->source_code($sourcecode);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);


$TEST_DESCRIPTION = "  Testing single quote strings";
$sourcecode = "'some char arr'";
$parsed_code = "<STRING>'some char arr'</STRING>";
$parsed_code .=  "\n";
$codeparser->source_code($sourcecode);
$codeparser->parse_code();
ok($codeparser->parsed_code() eq $parsed_code, $TEST_DESCRIPTION);
