#!/bin/sh

# Run all tests for codeprettifier and returns a sumary of it's execution

estimated_tests="0";
passed_tests="0";
failed_tests="0";

TESTS_DIR='t';
APACHE_TEST_FILE='t/ws/apache.t';

run_tests_from_file(){
    qtd=`cat $1 | grep 'Simple tests =>' | cut -d' ' -f5 | cut -d';' -f1`;
    echo "running $qtd test in $1";
    estimated_tests=$(($estimated_tests + $qtd));

    k="0";     
    for j in `perl -I lib/ $1 2>&1 | egrep -i 'ok\s|Failed\stest'|cut -d' ' -f1`
    do
	if [ $j = 'ok' ]
	then	   
	    k=$(($k + 1));
	    passed_tests=$(($passed_tests + 1));
	elif [ $j = 'not' ]
	then
	    k=$(($k + 1));
	    failed_tests=$(($failed_tests + 1));
	else
	    error="\n    Failed  $1  on test  $k";
	    fullinfo="$fullinfo$error";
	fi;

    done;
}

# codeprettifier library tests
for i in `ls $TESTS_DIR/*.t`
do    
    run_tests_from_file $i;
done;


# Optional tests
for i in $*
do
    # run apache ws tests
    if [ $i = '--run-apache-ws-tests' ]
    then
	run_tests_from_file $APACHE_TEST_FILE;
    fi
done;


total_tests=$(($passed_tests + $failed_tests));

echo "Tests summary for codeprettifier: ";
echo "  Estimated tests: " $estimated_tests;
echo "  Total tests: " $total_tests;
echo "  Tests ok: " $passed_tests;
echo "  Tests failed: " $failed_tests;

if [ $(($failed_tests > 0)) != '0' ]
then
    echo "";
    echo "  Full info: " $fullinfo;
    echo "\n";
    exit 1
else
    exit 0
fi;
