#!/usr/bin/env perl

# Copyright 2011 - 2014 Juca Crispim <juca@poraodojuca.net>

# This file is part of CodePrettifier.

# CodePrettifier is free software: you can redistribute it and/or modify	
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CodePrettifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with CodePrettifier.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;

use CodePrettifier::CodeParser;
use CodePrettifier::HtmlWriter;
    
our @KNOWN_PARAMETERS = ('--language', '--outfile', '--style-inline', '--without-line-number', '--help');

sub parse_command_line{

    my $parameters = {};

    #input file
    $parameters->{'inputfile'} = $ARGV[0];

    # Getting language for source code to be parsed
    grep { 
	if ($_ =~ m/--language=(\w+)/ ){
	    $parameters->{'language'} = $1;
	}
    }@ARGV;

    # Output file
    grep { 
	if ($_ =~ m/--outfile=(\w+)/ ){
	    $parameters->{'outfile'} = $1;
	}
    }@ARGV;
    if (!defined $parameters->{'outfile'}){
	$parameters->{'outfile'} = do_outfile_name($parameters->{'inputfile'});
    }

    # style-inline?
    $parameters->{'style_inline'} = 0;
    grep { 
	if ($_ =~ m/--style-inline/ ){
	    $parameters->{'style_inline'} = 1;
	}
    }@ARGV;

    # line number?
    $parameters->{'line_number'} = 1;
    grep { 
	if ($_ =~ m/--without-line-number/ ){
	    $parameters->{'line_number'} = 0;
	}
    }@ARGV;

    # help?
    $parameters->{'help'} = 0;
    grep { 
	if ($_ =~ m/--help/ ){
	    $parameters->{'help'} = 1;
	}
    }@ARGV;

    $parameters->{full_html} = 1;
    return $parameters;
}
	
sub read_input_file{
    my $filename = shift;
    open FILE, "<$filename" || die ("Problems while opening input file.\n");
    my $source_code = "";
    while(<FILE>){
	$source_code .= $_;
    }
    close FILE;
    return $source_code;
}

sub do_outfile_name{
    my $input_file = shift;
    my @filename = split('/', $input_file);
    my $outfilename = $filename[-1];
    $outfilename =~ s/\.(.*)/_$1\.html/;
    return $outfilename;
}

sub print_help_message{
    my $help_msg = "codeprettifier usage:\n";
    $help_msg .= " codeprettifier INPUTFILE --language=LANGUAGE [opts]\n";
    $help_msg .= "\n  --outfile=OUTFILE       - specify name for output file.\n";
    $help_msg .= "\n  --style-inlne           - write html style inline\n";
    $help_msg .= "\n  --without-line-number   - do not display line number\n";
    $help_msg .= "\n  --help                  - display this message and exit\n";

    print $help_msg;
}
    

my $input_params = parse_command_line();

if ($input_params->{'help'}){
    print_help_message();
    exit;
}

my $source_code = read_input_file($input_params->{'inputfile'});

my $code_parser = CodeParser->new($input_params->{'language'});
$code_parser->source_code($source_code);
my $parsed_code = $code_parser->parse_code();

my $html_writer = HtmlWriter->new();
$html_writer->do_html($parsed_code, {'style_inline' => $input_params->{'style_inline'},
				     'line_number' => $input_params->{'line_number'},
				     'full_html' => $input_params->{'full_html'},
		      });
$html_writer->write_html($input_params->{'outfile'});


