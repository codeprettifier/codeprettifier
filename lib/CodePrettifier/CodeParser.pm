package CodeParser;

# Copyright 2011 - 2014 Juca Crispim <juca@poraodojuca.net>

# This file is part of CodePrettifier.

# CodePrettifier is free software: you can redistribute it and/or modify	
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CodePrettifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with CodePrettifier.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;


# This class is used to parse a source code from some language.
# The languages available are the languages in languages/$LanguageName.pm
# This code works (more ou less) this way:
# Gets a string containing a source code (the type for the language must be
# specified on constructor) and parses it using SyntaticElement's subclasses
# for the specified language. 


# The class constructor
sub new{
    my $class = shift;
    my $type = shift || 'Language'; 
    my $self = {};
    $self->{TYPE} = $type;
    $self->{SOURCE_CODE} = undef;
    $self->{PARSED_CODE} = undef;

    bless($self, $class);
    $self->_get_language_for_this_type();

    return $self;
}

sub source_code{
    my $self = shift;
    if (@_){
	$self->{SOURCE_CODE} = shift;
	my $parsed_code = $self->_escape_lesser_greater_signs(
	    $self->{SOURCE_CODE});

	$self->{PARSED_CODE} = $parsed_code;
    }
    return $self->{SOURCE_CODE};
}

sub parsed_code{
    my $self = shift;
    $self->{PARSED_CODE} = shift if @_;
    return $self->{PARSED_CODE};
}

sub type{
    my $self = shift;

    $self->{TYPE} = shift if @_;
    return $self->{TYPE};
}

sub language{
    my $self = shift;
    return $self->{LANGUAGE};
}

sub parse_code{
    my $self = shift;

    my $code = $self->parsed_code();
    $code = $self->language()->parse_code($code);
    
    $self->parsed_code($code);
}
    

sub _get_language_for_this_type{
    # Gets an object representing the specified language.
    # This language object has some syntactic elements 
    # (instances from SyntacticElement) wich will be used
    # for parse the source code.
    my $self = shift;
    
    # Constructing a string...
    my $language = "CodePrettifier::Languages::" . $self->type();

    # ... eval'ing it.
    eval "use $language";
    die $@ if $@;
    # Again.
    my $expr = $self->type() . '->new()';
    # eval isn't evil!
    my $lang_obj = eval $expr;

    $self->{LANGUAGE} = $lang_obj;
}

sub _escape_lesser_greater_signs{
    # This function convert '<' and '>' signs
    # to &lt and &gt.
    # This function MUST be called on a cleane source code
    # never on parsed code;
    my $self = shift;
    my $code = shift;

    $code =~ s/</&lt;/sg;
    $code =~ s/>/&gt;/sg;
    
    return $code;
}

# must returns a true value
1;
