package Variable;

# Copyright 2011 - 2014 Juca Crispim <juca@poraodojuca.net>

# This file is part of CodePrettifier.

# CodePrettifier is free software: you can redistribute it and/or modify	
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CodePrettifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with CodePrettifier.  If not, see <http://www.gnu.org/licenses/>.


use CodePrettifier::SyntacticElements::SyntacticElement;
@ISA = (SyntacticElement);


sub new{
    my $class = shift;

    my $self = SyntacticElement->new(@_);
    $self->{TAGS} = ['<VARIABLE>', '</VARIABLE>'];
    
    return bless($self, $class);
}

# sub tagify{
#     my $self = shift;
#     die $self->SUPER::tagify(@_);
# }
