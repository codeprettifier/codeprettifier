package Keyword;

# Copyright 2011 - 2014 Juca Crispim <juca@poraodojuca.net>

# This file is part of CodePrettifier.

# CodePrettifier is free software: you can redistribute it and/or modify	
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CodePrettifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with CodePrettifier.  If not, see <http://www.gnu.org/licenses/>.


use CodePrettifier::SyntacticElements::SyntacticElement;
@ISA = (SyntacticElement);


sub new{
    my $class = shift;
    my $self = SyntacticElement->new(@_);
    $self->{TAGS} = ['<KEYWORD>', '</KEYWORD>'];
    
    return bless($self, $class);
}
    
sub tagify{
    my $self = shift;

    my $code = shift || die("I need something to tagify");
    my ($tag, $close_tag) = @{$self->{TAGS}};
    my $word = $self->word();
    $word = $self->escape_chars($word);
    $code =~ s/(^|\s|\()$word(:|\.|\s|,|;|\)|\(.*)/$1$tag$word$close_tag$2/sg;
    $code = $self->unescape_chars($code);
    return $code;
}
    
sub word{
    $self = shift;
    return $self->delimiter();
}

1;
