package CodePrettifier::WS::Apache::CodePrettifier;

# Copyright 2011 - 2014 Juca Crispim <juca@poraodojuca.net>

# This file is part of CodePrettifier.

# CodePrettifier is free software: you can redistribute it and/or modify	
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CodePrettifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with CodePrettifier.  If not, see <http://www.gnu.org/licenses/>.


use strict;
use warnings;

use Apache2::RequestRec ();
use Apache2::RequestIO ();
use Apache2::Request;
use APR::Table;
use Apache2::Const -compile => qw(OK);
use URI::Escape;

use CodePrettifier::CodeParser;
use CodePrettifier::HtmlWriter;

sub handler{
    my $request = shift;
    $request = Apache2::Request->new($request);
    $request->content_type('text/html');
    my $content_length = $request->headers_in->{'Content-Length'};
    if ($request->method() eq 'GET' || $content_length <= 0){
	return _show_help_message();
    }

    if (!defined $request->param('source_code')){
	return _show_help_message();
    }

    # parsing code
    my $code_parser = CodeParser->new($request->param('language'));
    $code_parser->source_code($request->param('source_code'));
    my $parsed_code = $code_parser->parse_code();
    
    my $html_writer = HtmlWriter->new();
    $html_writer->do_html($parsed_code, {'style_inline' => $request->param('style_inline'),
					 'line_number' => $request->param('line_number'),
					 'full_html' => $request->param('full_html'),
			  });
    print $html_writer->html_code();

    return Apache2::Const::OK;
}

sub _show_help_message{
    my @avaliable_languages = ('C', 'Java', 'Javascript', 'Perl', 'Php', 'Python');

    my $info_str = "<strong>CodePrettifier::WS</strong> - Send a POST request ";
    $info_str .=  "to this url and get a html with highlight syntax<br/><br/>";
    print $info_str;

    print "API params:<br/>";
    
    # source_code
    $info_str = "&nbsp;&nbsp;  source_code: REQUIRED - A string containig ";
    $info_str .= "the source code to be prettyfied<br/></br>";
    print $info_str;
    
    # language
    print "&nbsp;&nbsp;  language: OPTIONAL<br/>";
    print "&nbsp;&nbsp;&nbsp;&nbsp;  Avaliable languages: <br/>";
    print "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  "; 
    print  join(', ', @avaliable_languages) . "<br/><br/>";
    
    # style_inline
    print "&nbsp;&nbsp;  style_inline: OPTIONAL - Bool defaults to 0<br/><br/>";
    #line_number
    print "&nbsp;&nbsp;  line_number: OPTIONAL - Bool defaults to 1<br/><br/>";
    # full_htmlh
    print "&nbsp;&nbsp;  full_html: OPTIONAL - Bool defaults to 0<br/><br/>";
    return Apache2::Const::OK;
}

1;
