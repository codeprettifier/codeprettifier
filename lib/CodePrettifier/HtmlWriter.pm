package HtmlWriter;

# Copyright 2011 - 2014 Juca Crispim <juca@poraodojuca.net>

# This file is part of CodePrettifier.

# CodePrettifier is free software: you can redistribute it and/or modify	
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CodePrettifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with CodePrettifier.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;

use File::Basename;

use CodePrettifier::CodeParser;


sub new{
    my $class = shift;
    my $self = {};
    $self->{HTML_TAGS} = ['<span class="">', '</span>'];
    bless($self, $class);
    return $self;
}

sub tags{
    my $self = shift;
    return $self->{HTML_TAGS};
}


sub _do_classed_tag{
    # return a html tag (span) with a class name
    my $self = shift;
    my $type = shift;
    my $class_name = lc($type) || undef;
    my ($tag, $close_tag) = @{$self->tags()};
    my $classed_tag = $tag;
    $classed_tag =~ s/class=""/class="$class_name"/g;
    return $classed_tag
}


sub html_code{
    my $self = shift;
    return $self->{HTML_CODE};
}

sub do_html{
    my $self = shift;
    my $parsed_code = shift;
    # a hash reference.
    my $html_opts = shift;

    $self->{PARSED_CODE} = $parsed_code;
    $self->{HTML_CODE} = $self->{PARSED_CODE};

    # writting span with class
    my ($tag, $close_tag) = @{$self->tags()};
    $self->{HTML_CODE} =~ s/<(\w+)>(.*?)<\/\w+>/$self->_do_classed_tag($1)."$2$close_tag"/esg;

    # line numbers?
    if (defined $html_opts->{'line_number'} && $html_opts->{'line_number'}){
	$self->_insert_line_number();
    }
    
    if (defined $html_opts->{'full_html'} && $html_opts->{'full_html'}){
	$self->_insert_full_html();
    }
    else{
	$self->_insert_pre_and_div();
    }

    # style inline?
    if (defined $html_opts->{'style_inline'} && $html_opts->{'style_inline'}){
	$self->_do_inline_style();
    }
    
    my $html_code = $self->html_code();
    # :/
    $html_code =~ s/\t/        /g;
    $self->{HTML_CODE} = $html_code;
}
    
    
sub write_html{
    my $self = shift;
    my $filename = shift;

    my $html_template = $self->_open_template_file();

    my $html_code = $self->html_code();
    
    open FILE, ">$filename" || die ("Problems while opening file.\n");
    print FILE $html_code;
    close FILE;
}

sub _insert_line_number{
    my $self = shift;
    my $html_code = $self->html_code();

    my @html_code_lines = split("\\n", $html_code);
    my $numbered_html_code = '';
    my $i = 1;
    map{
	$numbered_html_code .= '<span class="line_number">' . "$i</span>$_\n";
	$i += 1;
    }@html_code_lines;
    
    $self->{HTML_CODE} = $numbered_html_code;
}

sub _insert_full_html{
    my $self = shift;
    
    my $html_template = $self->_open_template_file();

    my $html_code = $self->html_code();
    
    $html_code = "<pre>\n" . $html_code . "</pre>\n";
    $html_template =~ s/(<title>)(<\/title>)/$1code prettyfier$2/;
    $html_template =~ s/<div id="source_code"><\/div>/<div id="source_code">$html_code<\/div>/sg;

    $self->{HTML_CODE} = $html_template;
}

sub _insert_pre_and_div{
    my $self = shift;

    my $html_code = $self->html_code();
    $html_code = "<pre>" . $html_code . "</pre>";
    $html_code = '<div id="source_code">' . $html_code . "</div>";
    
    $self->{HTML_CODE} = $html_code;
}
    
sub _open_template_file{
    my $self = shift;

    my $template_file = dirname(__FILE__) . '/templates/sourcecode2html.html';
    open FILE, "<$template_file" || die ("Problems while opening template file.\n");
    my $html_template = "";
    while(<FILE>){
	$html_template .= $_;
    }
    return $html_template;
}

sub _do_inline_style{
    my $self = shift;
    my $html_code = $self->html_code();
    
    my $html_template = $self->_open_template_file();
    $html_template =~ /<style>(.+)<\/style>/sg;
    
    my $style_rules = $1 if $1;
    if (!$style_rules){
	die("No style found\n");
    }

    my $elements_style = $self->_get_elements_style($style_rules);

    map{
	$html_code =~ s/$_/$elements_style->{$_}/sg;
    }(keys %{$elements_style});
    
    $self->{HTML_CODE} = $html_code;
}

sub _get_elements_style{
    my $self = shift;
    my $style_rules = shift || die("I need style rules\n");
    
    my $style_classes = {};
    while ($style_rules =~ m/\.(.\w+?){(.*?)}/sg){
	$style_classes->{$1} = $2;
    }

    my $elements_ids = {};
    while ($style_rules =~ m/#(\w+?){(.*?)}/sg){
	$elements_ids->{$1} = $2;
    }

    my $style_elements = {};
    map{
	my $class_element = 'class="'. $_ .'"';
	my $style_element = 'style="' . $style_classes->{$_} . '"';
	$style_element =~ s/(\s+)/ /g;
	$style_element =~ s/(\n+)/ /g;
	$style_elements->{$class_element} = $style_element;
    }(keys %{$style_classes});

    map{
	my $id_element = 'id="'. $_ .'"';
	my $style_element = 'style="' . $elements_ids->{$_} . '"';
	$style_element =~ s/(\s+)/ /g;
	$style_element =~ s/(\n+)/ /g;
	$style_elements->{$id_element} = $style_element;
    }(keys %{$elements_ids});

    return $style_elements;
}
1;
