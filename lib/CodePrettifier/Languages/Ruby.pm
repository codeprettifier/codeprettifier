package Ruby;

# Copyright 2011 - 2014 Juca Crispim <juca@poraodojuca.net>

# This file is part of CodePrettifier.

# CodePrettifier is free software: you can redistribute it and/or modify	
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CodePrettifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with CodePrettifier.  If not, see <http://www.gnu.org/licenses/>.


use CodePrettifier::Languages::Language;

@ISA = (Language);

sub new{
    my $class = shift;

    my @reserved_words = qw(alias and BEGIN begin break case class def
                            defined? do      else    elsif
			  END     end     ensure  false   for     if

			  in      module  next    nil     not     or
			  redo    rescue  retry return  self    super   then
			  true    undef   unless  until   when while   yield);

    my $comment_delimiters = ["#"];
    my $string_delimiters = [qr{(^|\s*?)(.*?)('.*?'|".*?")(.*\n)}x,];
    my $function_name_delimiters = [qr{(^|\s+?)(def\s+)(\w+)(\W|\s)}x];
    my $type_name_delimiters = [qr{(^|\s+?|\.|\()(.*?)([A-Z]+\w+)(\W|\s|$)}x];
    my $variable_delimiters = [qr{(^|\s+)(\s*)(\@\w+)(\W|\s)}x];

    my $elements_list = [['TypeName', $type_name_delimiters],
			 ['FunctionName', $function_name_delimiters],
			 ['Keyword', \@reserved_words],
			 ['Comment', $comment_delimiters],
			 ['String', $string_delimiters],
			 ['Variable', $variable_delimiters],
	];

    my $self = Language->new($elements_list);

    bless($self, $class);

    return $self;
}
