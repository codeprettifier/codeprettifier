package C;

# Copyright 2011 - 2014 Juca Crispim <juca@poraodojuca.net>

# This file is part of CodePrettifier.

# CodePrettifier is free software: you can redistribute it and/or modify	
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CodePrettifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with CodePrettifier.  If not, see <http://www.gnu.org/licenses/>.


use CodePrettifier::Languages::Language;

@ISA = (Language);

sub new{
    my $class = shift;

    my @key_words = qw(auto break case Complex const continue
			    default do else enum extern for goto
			    if _Imaginary inline register restrict 
			    return short signed sizeof static struct switch
			    typedef union volatile while);

    my $comment = [['/*', '*/'], '//'];
    my $string = [qr{(^|\s*?)(.*?)('.*?'|".*?")(.*\n)}x,
	];

    my $type_names = ['int', 'float', 'long', 'char', 'void', 
		      '_Bool', 'double', 'unsigned'];
    my $type_names_str = join('|', @{$type_names});

    my $type_name = [qr{(^|\s+)(.*?)($type_names_str)(.*)}x,];

    my $type_names_str = join('\s+|', @{$type_names});
    my $function_name = [qr{(^|\s+)($type_names_str\s+)(\w+)(\(|\{|\s+)}x,
			 # this regex is for #include. It's here because
			 # highlight is the same for functions and includes
			 qr{(.*)(.*)(#.+?)(\s+?)},];
	
    my $variable = [qr{(^|\s+)($type_names_str\s+)(\w+)(.*)}x,];

    my $elements_list = [['FunctionName', $function_name],
			 ['Variable', $variable],
			 ['Keyword', \@key_words],
			 ["Comment", $comment],
			 ['String', $string],
			 ['TypeName', $type_name],
	];

    my $self = Language->new($elements_list);

    bless($self, $class);

    return $self;
}


sub parse_code{
    my $self = shift;
    my $code = shift || die "I need some code to parse!";
    
    $code = $self->_tagify_vars($code);
    return $self->SUPER::parse_code($code);
}
    
sub _tagify_vars{
    # ugly hack for handle variables
    # this is here because I can't handle thing like
    # int a, b, some_int; with one regex.
    
    my $self = shift;
    my $code = shift || die "I need some code to parse!";
    my $pcode = $code;

    my $type_names = ['int', 'float', 'long', 'char', 'void', 
		      '_Bool', 'double', 'unsigned'];
    my $type_names_str = join('\s+|', @{$type_names});
    my $element = $self->_get_syntactic_element_from_name('Variable', 'fake');
    my  ($tag, $close_tag) = $element->tags();

    while ($code =~ m/(^|\s+)($type_names_str\s+)(.*;)/sg){
	# if something like int a, b, c
	# $3 is a, b, c
	my $vars = $3 if defined $3;
	if (defined $vars && $vars){
	    my $tagged_vars = $vars;
	    # now, putting tags around each one of vars, ie
	    # <VARIABLE>a</VARIABLE>, <VARIABLE>b</VARIABLE>, <VARIABLE>c</VARIABLE>
	    $tagged_vars =~ s/(\s*|,)(\w+)(,\s*|\s+|\[|;)/$1$tag$2$close_tag$3/sg;
	    # here is another thing wrong this these regexes. This corrects
	    # an error like this: <VARIABLE>a</VARIABLE>=<VARIABLE>10</VARIABLE>
	    $tagged_vars =~ s/(=\s*)<VARIABLE>(.*?)<\/VARIABLE>/$1$2/sg;
	    $pcode =~ s/$vars/$tagged_vars/s;
	}
    }
    return $pcode;
}
1;

