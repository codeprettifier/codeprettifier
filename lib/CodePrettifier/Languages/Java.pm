package Java;

# Copyright 2011 - 2014 Juca Crispim <juca@poraodojuca.net>

# This file is part of CodePrettifier.

# CodePrettifier is free software: you can redistribute it and/or modify	
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CodePrettifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with CodePrettifier.  If not, see <http://www.gnu.org/licenses/>.

use CodePrettifier::Languages::Language;

# Decorator in python has the same syntax as annotations in java
# so, using it for annotations here.
#use CodePrettifier::Languages::SyntacticElements::Python::DecoratorSign;

@ISA = (Language);

sub new{
    my $class = shift;

    my @key_words = qw(abstract continue for new switch
		       assert default goto package synchronized
		       do if private this
		       break implements protected throw
		       else import public throws
		       case enum instanceof return transient
		       catch extends short try
		       final interface static
		       class finally strictfp volatile
		       const native super while
	);

    my $comment = [['/*', '*/'], '//'];
    my $string = [qr{(^|\s*?)(.*?)('.*?'|".*?")(.*\n)}x,];

    my $constants = [qr{(^|\s+|\()(\s*)(null|true|false)(.*)}x];
    my $type_name = [qr{(^|\s+|\.|\()(\s*)([A-Z]+\w+|int|float|long|char|void|boolean|double|byte)(\s+|\.|;)}x];
    my $function_name = [qr{(^|\s+|\.)(\w+\s+)(\w+)(\s*?\()}x];
    my $variable = [qr{(^|\s+|\.|\()([A-Z]\w+\s+|int\s+|float\s+|long\s+|char\s+|void\s+|boolean\s+|double\s+|byte\s+)(\w+)(\s*?|;|,)}x];
    my $annotation_signs = ['@'];
    
    # here the order is important. Maybe not forever, just maybe...

    my $elements = [['Keyword', \@key_words],
		    ['FunctionName', $function_name],
		    ['Variable', $variable],
		    ['Comment', $comment],
		    ['String', $string],
		    ['TypeName', $type_name],
	];    

    my $self = Language->new($elements);

    bless($self, $class);


    return $self;
}
    
1;
