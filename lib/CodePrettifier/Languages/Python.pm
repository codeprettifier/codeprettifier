package Python;

# Copyright 2011 - 2014 Juca Crispim <juca@poraodojuca.net>

# This file is part of CodePrettifier.

# CodePrettifier is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CodePrettifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with CodePrettifier.  If not, see <http://www.gnu.org/licenses/>.


use CodePrettifier::Languages::Language;


@ISA = (Language);

sub new{
    my $class = shift;

    my $key_words = ['and', 'as', 'assert', 'break', 'class', 'continue',
		     'def', 'del', 'elif', 'else', 'except', 'exec',
		     'finaly', 'for', 'from', 'global', 'if', 'import', 'in',
		     'is', 'lambda', 'not', 'or', 'pass', 'print', 'async', 'await',
		     'raise', 'return', 'self', 'try', 'while', 'with', 'yield'];

    my $builtin_functions = ['abs', 'all', 'any', 'basestring', 'bin', 'bool',
			     'bytearray', 'callable', 'chr', 'classmethod', 'cmp',
			     'compile', 'complex', 'delattr', 'dict', 'dir', 'divmod',
			     'enumerate', 'eval', 'execfile', 'file', 'filter', 'float',
			     'format', 'frozenset', 'getattr', 'globals', 'hasattr',
			     'hash', 'help', 'hex', 'id', 'input', 'int', 'isinstance',
			     'issubclass', 'iter', 'len', 'list', 'locals', 'long',
			     'map', 'max', 'memoryview', 'min', 'next', 'object', 'oct',
			     'open', 'ord', 'pow', 'print', 'property', 'range',
			     'raw_input', 'reduce', 'reload', 'repr', 'reversed', 'round',
			     'set', 'setattr', 'slice', 'sorted', 'staticmethod', 'str',
			     'sum', 'super', 'tuple', 'type', 'unichr', 'unicode', 'vars',
			     'xrange', 'zip', '__import__', 'apply', 'buffer', 'coerce',
			     'intern'];

    my $builtin_functions_str = join('|', @{$builtin_functions});
    my $comment = ["#"];
    my $string = [qr{(^|\s*?)(.*?)('''.*?'''|""".*?""")(.*?)}xs,
		  qr{(^|\s*?)(.*?)('.*?'|".*?")(.*\n)}x,
	];
    my $function_name = [qr{(^|\s+?)(def\s+)(\w+)(\W|\s)}x,
	                 qr{(^|\s+?)(\s*|\()($builtin_functions_str)(\W|\s)}x,];
    my $type_name = [qr{(^|\s+?)(class\s+)(\w+)(\W|\s)}x,
		     # this regex is for decorators
                     qr{(^|\s+?)(.*)(\@\w+)(.*)}x,];
    my $variable = [qr{(^|\s+)(\s*)(\w+)(\s*?=)}x,];

    my $elements_list = [['TypeName', $type_name],
			 ['FunctionName', $function_name],
			 ['Keyword', $key_words],
			 ['Comment', $comment],
			 ['String', $string],
			 ['Variable', $variable],];

    my $self = Language->new($elements_list);

    bless($self, $class);

    return $self;
}

sub _clean_multi_line_strings{
    # multi line strings in python must be cleaned first;
    # ugly thing

    my $self = shift;
    my $code = shift || die "I need something to clean!";

    # aqui @{$self} é uma lista com os syntactic_elements
    #que tem na linguagem
    map{
	my ($tag, $ctag) = $_->tags();
	my $regex = qr{$tag$tag(''|"")$ctag('|")}x;
	$code =~ s/$regex/$tag$1$2/sg;

	$regex = qr{$tag(''|"")$ctag('|")$ctag}x;
	$code =~ s/$regex/$1$2$ctag/sg;
    }@{@{$self}[4]};

    return $code;
}

sub _clean_named_args{
    # here I clean named args which are identified
    # as variable names.
    # something like:
    # run(1, <VARIABLE>a</VARIABLE>=2)
    # and returns
    # run(1, a=2)

    my $self = shift;
    my $code = shift || die "I need something to clean!";

    map{
	my ($tag, $ctag) = $_->tags();
	my $regex = qr{(\(.*?\))}xs;
	$code =~ m/$regex/m;
	my $parens = $1;
	my $cleaned_parens = $parens;

	$regex = qr{($tag)(.*?)($ctag)}xs;
	$cleaned_parens =~ s/$regex/$2/mg;
	if ($parens ne $cleaned_parens){
	    $code =~ s/\Q$parens/$cleaned_parens/mg;
	    $code =~ s/\(\((.*)\)\)/($1)/ms;
	}
    }@{@{$self}[5]};

    return $code;
}

sub _clean_parsed_code{
    my $self = shift;
    my $code = shift || die "I need something to clean!";

    $code = $self->_clean_multi_line_strings($code);
    $code = $self->_clean_named_args($code);

    return $self->SUPER::_clean_parsed_code($code);
}

1;
