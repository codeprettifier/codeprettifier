package Php;

# Copyright 2011 - 2014 Juca Crispim <juca@poraodojuca.net>

# This file is part of CodePrettifier.

# CodePrettifier is free software: you can redistribute it and/or modify	
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CodePrettifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with CodePrettifier.  If not, see <http://www.gnu.org/licenses/>.

use CodePrettifier::Languages::Language;

@ISA = (Language);

sub new{
    my $class = shift;

    # Not all here a true reserved words. Somethings are
    # built in functions, and even the <?php sign. But are here
    # because i want it with the same colors as reserved words.
    my @reserved_words = ('and', 'as', 'abstract', 'array', 'break', 'class', 
			  'continue', 'case', 'catch', 'clone', 'const', 
			  'declare', 'default', 'do', 'elseif', 'else', 
			  'enddeclare', 'endfor', 'endforeach', 'endif', 
			  'endswitch', 'endwhile', 'extends', 'final', 'foreach',
			  'function', 'for', 'goto', 'global', 'if', 'implements', 
			  'interface', 'instanceof', 'namespace', 'new', 'private',
			  'protected', 'public', 'static', 'switch', 'throw', 
			  'try', 'use', 'var', 'while', 'xor', 'die', 'echo',
			  'return', '<?php', '?>');

    my $builtin_functions = ['empty', 'exit', 'eval', 'isset', 'list', 'include',
	                     'include_once', 'require', 'require_once'];

    my $comment_delimiters = ["//", ['/*', '*/']];
    my $string_delimiters = [qr{(^|\s*?)(.*?)('.*?'|".*?")(.*\n)}x];
    my $function_name_delimiters = [qr{(^|\s+?)(function\s+)(\w+)(\W|\s)}x];
    my $variable_delimiters = [qr{(^|\s*?)(\$)(\w+)(\W*?)}x];;
    my $type_name_delimiters = [qr{(^|\s+?)(class\s+)(\w+)(\W|\s)}x];

    my $elements_list = [ ['TypeName', $type_name_delimiters],
			  ['FunctionName', $function_name_delimiters],
			  ['Keyword', \@reserved_words],
			  ['Comment', $comment_delimiters],
			  ['String', $string_delimiters],
	                  ['Variable', $variable_delimiters],];

    my $self = Language->new($elements_list);

    bless($self, $class);

    return $self;
}
