package Javascript;

# Copyright 2011 - 2014 Juca Crispim <juca@poraodojuca.net>

# This file is part of CodePrettifier.

# CodePrettifier is free software: you can redistribute it and/or modify	
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CodePrettifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with CodePrettifier.  If not, see <http://www.gnu.org/licenses/>.


use CodePrettifier::Languages::Language;

@ISA = (Language);

sub new{
    my $class = shift;

    my @reserved_words = qw(abstract as boolean break byte case catch
			  char class continue const debugger default
			  delete do double else enum export extends 
			  false final finally float for function goto 
			  if implements import in instanceof int interface 
			  is long namespace native new null package private 
			  protected public return short static super switch
			  synchronized this throw throws transient true try
			  typeof use var void volatile while with
	);

    my $comment_delimiters = [['/*', '*/'], '//'];
    my $string_delimiters = [qr{(^|\s*?)(.*?)('.*?'|".*?")(.*\n)}x,];

    my $variable_delimiters = [qr{(^|\s+?)(var\s+?)(\w+)(\W+)}x];
    my $function_name_delimiters = [qr{(^|\s+?|.*?)(function\s+)(\w+)(\W|\s)}x];

    my $elements_list = [['Variable', $variable_delimiters], 
			 ['FunctionName', $function_name_delimiters],
			 ['Keyword', \@reserved_words],
			 ['Comment', $comment_delimiters],
			 ["String", $string_delimiters],
	];

    my $self = Language->new($elements_list);

    bless($self, $class);


    return $self;
}
    
1;
