package Perl;

# Copyright 2011 - 2014 Juca Crispim <juca@poraodojuca.net>

# This file is part of CodePrettifier.

# CodePrettifier is free software: you can redistribute it and/or modify	
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CodePrettifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with CodePrettifier.  If not, see <http://www.gnu.org/licenses/>.


use CodePrettifier::Languages::Language;

@ISA = (Language);

sub new{
    my $class = shift;

    my @key_words = qw(continue default do else elsif eq err exp
			    for foreach ge given gt if le lock lt ne print
			    no or package sub unless until die when while use
                            return);

    my $comment = ["#"];
    my $string = [qr{(^|\s*?)(.*?)('.*?'|".*?")(.*\n)}x,
	];
    my $function_name = [qr{(^|\s+?)(sub\s+)(\w+)(\W|\s)}x];
    my $variable = [qr{(^|\s*?)(\$|\@|\%)(\w+)(\W*?)}x];

    my $elements_list = [ #[$type_name_delimiters, TypeNameDelimiter],
			  ['FunctionName', $function_name],
			  ['Keyword', \@key_words],
			  ['Comment', $comment],
			  ["String", $string],
	                  ['Variable', $variable]];

    my $self = Language->new($elements_list);

    bless($self, $class);

    return $self;
}

    
1;
