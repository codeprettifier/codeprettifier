package SyntacticElement;

# Copyright 2011 - 2014 Juca Crispim <juca@poraodojuca.net>

# This file is part of CodePrettifier.

# CodePrettifier is free software: you can redistribute it and/or modify	
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CodePrettifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with CodePrettifier.  If not, see <http://www.gnu.org/licenses/>.


use strict;
use warnings;

# Note: Syntactic Element in the sense used in this class
# isn't a syntactic element in the sense  usually used for
# programming language syntax.
# Here, a syntactic element is simply something I want to highlight.

sub new{
    my $class = shift;
    my $self = {};

    return bless($self, $class);
}

sub tagify{
    my $self = shift;
    my $code = shift || die 'I need something to tagify';
    
    # This regex passed to tagify has a rule:
    # It must have 4 groups: 
    # $1 and $2 goes before the tags, $3 inside tags 
    # and $4 after the tags
    my $regex = $self->delimiter();
    my ($tag, $ctag) = $self->tags();
    $code =~ s/$regex/$1$2$tag$3$ctag$4/sg;
    return $code;
}
    
sub escape_chars{
    # Escape special chars to be used
    # on regex;
    my $self = shift;
    my $expr = shift || die "I need some char to escape";

    $expr =~ s/\//\\\//sg;
    $expr =~ s/\*/\\\*/sg;
    $expr =~ s/\(/\\\(/sg;
    $expr =~ s/\)/\\\)/sg;
    $expr =~ s/\]/\\\]/sg;
    $expr =~ s/\[/\\\[/sg;
    $expr =~ s/\$/\\\$/sg;
    $expr =~ s/\?/\\\?/sg;
    $expr =~ s/\+/\\\+/sg;
    $expr =~ s/\@/\\\@/sg;
    $expr =~ s/\%/\\\%/sg;
    $expr =~ s/\^/\\\^/sg;

    return $expr;
}

sub unescape_chars{
    my $self = shift;
    my $expr = shift || die "I need some char to unescape";

    $expr =~ s/\\\//\//sg;
    $expr =~ s/\\\*/\*/sg;
    $expr =~ s/\\\(/\(/sg;
    $expr =~ s/\\\)/\)/sg;
    $expr =~ s/\\\]/\]/sg;
    $expr =~ s/\\\[/\[/sg;
    $expr =~ s/\\\$/\$/sg;
    $expr =~ s/\\\?/\?/sg;
    $expr =~ s/\\\+/\+/sg;
    $expr =~ s/\\\@/\@/sg;
    $expr =~ s/\\\%/\%/sg;
    $expr =~ s/\\\^/\^/sg;

    return $expr;
}

sub tags{
    my $self = shift;
    return @{$self->{TAGS}};
}

sub delimiter{
    my $self = shift;

    $self->{DELIMITER} = shift if @_;
    return $self->{DELIMITER};
}

1;
