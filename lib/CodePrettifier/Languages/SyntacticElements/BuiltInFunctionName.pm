package BuiltInFunctionName;

# Copyright 2011 - 2014 Juca Crispim <juca@poraodojuca.net>

# This file is part of CodePrettifier.

# CodePrettifier is free software: you can redistribute it and/or modify	
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CodePrettifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with CodePrettifier.  If not, see <http://www.gnu.org/licenses/>.


use CodePrettifier::Languages::SyntacticElements::FunctionNameDelimiter;
@ISA = (SyntacticElement);


sub new{
    my $class = shift;
    my $delimiter = shift if @_ || undef;

    my $self = SyntacticElement->new(@_);
    $self->{DELIMITER} = $delimiter;
    $self->{TAGS} = ['<FUNCTIONNAME>', '</FUNCTIONNAME>'];
    
    return bless($self, $class);
}

sub function_name{
    my $self = shift;
    return $self->{DELIMITER};
}

sub tagify{
    my $self = shift;
    my $code = shift;
    my ($tag, $close_tag) = @{$self->{TAGS}};
    my $function_name = $self->function_name();
    $code =~ s/(^|\s+|\(|\{)$function_name(\(|\s+?)/$1$tag$function_name$close_tag$2/sg;
    return $code;
}

1;
