package CommentDelimiter;

# Copyright 2011 - 2014 Juca Crispim <juca@poraodojuca.net>

# This file is part of CodePrettifier.

# CodePrettifier is free software: you can redistribute it and/or modify	
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CodePrettifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with CodePrettifier.  If not, see <http://www.gnu.org/licenses/>.


use CodePrettifier::Languages::SyntacticElements::SyntacticElement;
@ISA = (SyntacticElement);


sub new{
    my $class = shift;
    my $delimiter = shift if @_ || undef;

    my $self = SyntacticElement->new(@_);
    $self->{DELIMITER} = $delimiter;
    $self->{TAGS} = ['<COMMENT>', '</COMMENT>'];
    
    return bless($self, $class);
}
    
sub tagify{
    my $self = shift;
    my $code = shift;
    my ($tag, $close_tag) = @{$self->{TAGS}};
    if (ref($self->delimiter()) eq 'ARRAY'){
	# Tagfy comments where the beginning and the ending 
	# signs are different each other.
	# i.e. c-like comments: /* comment here */
	my ($bsign, $esign) = @{$self->delimiter()};

	# :/
	$bsign = $self->escape_chars($bsign);
	$esign = $self->escape_chars($esign);

	$code =~ s/$bsign(.*?)$esign/$tag$bsign$1$esign$close_tag/sg;
	$code = $self->unescape_chars($code);

    }
    else{
	my $delimiter = $self->delimiter();
	$delimiter = $self->escape_chars($delimiter);
	my $new_code = $code;
	while($new_code =~ m/$delimiter(.*)/g){
	    $content = $1 if $1 || next;
	    $content = $self->escape_chars($content);
	    if (!$self->_inside_comments($content, $new_code)){
		$code =~ s/$delimiter$content/$tag$delimiter$content$close_tag/g;
	    }
	}
    }
    $code = $self->unescape_chars($code);

    if ($code !~ m/\n$/){
	$code .= "\n";
    }
    return $code;
}

sub _inside_comments{
    my $self = shift;
    my $cont = shift;
    my $code = shift;
#    $cont = $self->escape_chars($cont);

    my ($tag, $close_tag) = @{$self->{TAGS}};
    while ($code =~ m/$tag(.*?)$close_tag/sg){
	my $tmp_content = $self->escape_chars($1);
	if (index($tmp_content, $cont) >= 0){
	    return 1;
	}
    }
    return 0;
}



1;
