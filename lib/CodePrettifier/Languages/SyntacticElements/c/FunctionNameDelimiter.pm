package c::FunctionNameDelimiter;

# Copyright 2011 - 2014 Juca Crispim <juca@poraodojuca.net>

# This file is part of CodePrettifier.

# CodePrettifier is free software: you can redistribute it and/or modify	
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CodePrettifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with CodePrettifier.  If not, see <http://www.gnu.org/licenses/>.


use CodePrettifier::Languages::SyntacticElements::FunctionNameDelimiter;
@ISA = (FunctionNameDelimiter);

# Delimiters for function definitions in c a predefined type names
# like int, char, float and  or { - is { right? 

sub function_type{
    my $self = shift;
    return $self->delimiter();
}


sub tagify{
    # Very strange regex for variables on c
    # It must be better.
    my $self = shift;
    my $code = shift;
    my ($tag, $close_tag) = @{$self->{TAGS}};
    my $function_type = $self->function_type();
    $function_type = $self->escape_chars($function_type);

    # Ugly regex. It should not handle my own tags...
    my $regex = qr{(^|\s+)
                   ($function_type)
                   (\s+?)
                   (\w+)(\(|\{)}x;
    
    $code =~ s/$regex/$1$2$3$tag$4$close_tag$5/sg;
    $code = $self->unescape_chars($code);
    return $code;
}
    

