package c::VariableDelimiter;

# Copyright 2011 - 2014 Juca Crispim <juca@poraodojuca.net>

# This file is part of CodePrettifier.

# CodePrettifier is free software: you can redistribute it and/or modify	
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CodePrettifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with CodePrettifier.  If not, see <http://www.gnu.org/licenses/>.


use CodePrettifier::Languages::SyntacticElements::VariableDelimiter;
@ISA = (VariableDelimiter);

# Delimiters for variable (definitions) in c a predefined type names
# like int, char, float...

sub var_type{
    my $self = shift;
    return $self->delimiter();
}


sub tagify{
    # Very strange regex for variables on c
    # It must be better.
    my $self = shift;
    my $code = shift;
    my ($tag, $close_tag) = @{$self->{TAGS}};
    my $var_type = $self->var_type();
    $var_type = $self->escape_chars($var_type);
    my $new_code = $code;

    # Ugly regex. It should not handle my own tags...
    # How does function and type names worked without this?
    while ($code =~ m/(^|\s+)$var_type(\s+?)(.*;)/g){
	my $var_names = $3 if defined $3;
	if (defined $var_names && $var_names){
	    my $tagged_vars = $var_names;
	    $var_names = $self->escape_chars($var_names);
	    $tagged_vars =~ s/(\s*|,)(\w+)(,\s*|\s+|\[|;)/$1$tag$2$close_tag$3/sg;
	    # bad regex since the `while`, so correcting wrong things now.
	    # This code corrects the problem with int a = 10;
	    # both, 'a' and '10' are being tagified as variable.
	    $tagged_vars =~ s/(=\s*)<VARIABLENAME>(.*?)<\/VARIABLENAME>/$1$2/sg;
	    $new_code =~ s/$var_names/$tagged_vars/s;
	}
    }
    $code = $self->unescape_chars($new_code);
    return $code;
}
    

