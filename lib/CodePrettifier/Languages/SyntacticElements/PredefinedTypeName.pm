package PredefinedTypeName;

# Copyright 2011 - 2014 Juca Crispim <juca@poraodojuca.net>

# This file is part of CodePrettifier.

# CodePrettifier is free software: you can redistribute it and/or modify	
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CodePrettifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with CodePrettifier.  If not, see <http://www.gnu.org/licenses/>.


use CodePrettifier::Languages::SyntacticElements::TypeNameDelimiter;
@ISA = (TypeNameDelimiter);


sub tagify{
    my $self = shift;
    my $code = shift;
    my ($tag, $close_tag) = @{$self->{TAGS}};
    my $type_name = $self->predefined_type_name();
    $type_name = $self->escape_chars($type_name);
    $code =~ s/(^|\s+|\()$type_name(\s+|\(|\{|:|\n|\))/$1$tag$type_name$close_tag$2/sg;
    $code = $self->unescape_chars($code);
    return $code;
}

sub predefined_type_name{
    my $self = shift;
    return $self->delimiter();
}

1;
