package Language;

# Copyright 2011 - 2014 Juca Crispim <juca@poraodojuca.net>

# This file is part of CodePrettifier.

# CodePrettifier is free software: you can redistribute it and/or modify	
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# CodePrettifier is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with CodePrettifier.  If not, see <http://www.gnu.org/licenses/>.

use strict;
use warnings;


sub new{
    my $class = shift;
    my $elements = shift;
    my $self = [];
    bless($self, $class);
    map{
	my ($element_name, $element_delimiter) = (@{$_}[0], @{$_}[1]);
	$self->_create_syntactic_element($element_name, $element_delimiter);
    }@{$elements};
    return $self;
}

sub parse_code{
    # Parses the source code recived and creates something like:
    # <KEYWORD>if</KEYWORD> ($string = <STRING>'something'</STRING>) ... 

    my $self = shift;
    my $code = shift || die "I need some code to parse!";
    map{
	# Uses the SyntacticElement from the language to tagify the 
	# syntactic elements.
	foreach my $syntactic_element (@{$_}){
	    $code = $syntactic_element->tagify($code);
	}
    }@{$self};

    $code = $self->_clean_parsed_code($code);
    return $code;
}

sub _clean_parsed_code{
    # It's used for clean a code that is inside a tag
    # Gets something like:
    # <STRING>" Here is a string <RESERVED>for</RESERVED> something"</STRING>
    # and returns:
    # <STRING>" Here is a string for something"</STRING>

    my $self = shift;
    my $code = shift || die "I need something to clean!";
    
    map{
	foreach my $syntactic_element(@{$_}){
	    my ($tag, $close_tag) = $syntactic_element->tags();
	    $code =~ s/$tag(.*?)($close_tag)/"$tag".$self->_strip_tags($1)."$2"/esg;
	}
    }@{$self};


    return $code;
}

sub _strip_tags{
    my $self = shift;
    
    my $line = shift;
    $line =~ s/(<\w+>|<\/\w+>)//sg;
    return $line;
}

sub _create_syntactic_element{
    my $self = shift;
    my $element_name = shift;
    # here is an array with the regex used on language definition
    my $element_delimiter_list = shift;
    my $syntactic_element_list = [];

    map{
	my $syntactic_element = $self->_get_syntactic_element_from_name(
	    $element_name, $_);
	push(@{$syntactic_element_list}, $syntactic_element);
    }@{$element_delimiter_list};
    push(@{$self}, $syntactic_element_list);
}

sub _get_syntactic_element_from_name{
    my $self = shift;
    my $element_name = shift || die 'I need a name\n!';
    my $element_delimiter = shift || die 'I need delimiters for an element';

    my $element = 'CodePrettifier::SyntacticElements::' . $element_name;
    
    eval "use $element";
    die $@ if $@;

    my $expr = $element_name . '->new($element_delimiter)';
    my $obj = eval $expr;
    return $obj;
}
	
1;
